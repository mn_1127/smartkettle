     def label = "mydev-${UUID.randomUUID().toString()}"
    def img_version = '${img_version}'
    def project_name = "smart-kettle-front"
    def img_name = "kettle-admin"
    def name_space = "yaukie"
    def setting_name = "settings"
    def repo_url = "registry.cn-qingdao.aliyuncs.com"
    def user_name = '${docker_user_name}'
    def pass_word = '${docker_pass_word}'
     node {

       stage('Preparation') {
                           echo 'ready to pull project_name....'
                           checkout(
                                   [$class                           : 'GitSCM', branches: [[name: '*/master']],
                                    doGenerateSubmoduleConfigurations: false,
                                    extensions                       : [],
                                    submoduleCfg                     : [],
                                    userRemoteConfigs                : [[credentialsId: '67383ec2-d738-4632-b4b8-36c70180efb0',
                                                                         url          : 'https://gitee.com/yaukie/x-smart-kettle-front.git']]]
                           )
                           echo 'repo_url has pulled....'
                       }


         stage('Build') {
             echo 'ready to build project ...... '
             sh 'node -v'
             sh 'npm install -g yarn -registry=https://registry.npm.taobao.org'
             sh 'yarn -v'
             sh 'yarn install --pure-lockfile'
             sh 'yarn  && yarn build';
             echo 'project ' + project_name + 'buid successfully ..... '
         }

         stage('DockerBuild') {
             echo 'ready to push img....'
             dir("docker/") {
                 sh 'ls -al'
                 sh '$(if [ ! -d "kettle-admin" ]; then mkdir "kettle-admin"; fi)'
                 sh 'ls -al '
                 echo project_name + 'ready to login aliyuncs....'
             }
             sh 'docker login -u ' + user_name + ' -p ' + pass_word + ' ' + repo_url
             echo 'aliyuncs has logined successfully....'
             echo 'ready to rm  kettle-admin ...'
             sh 'docker stop $(docker ps -a  | awk  \'{print $1,$NF}\' | grep -v grep | grep \'kettle-admin\'| awk -F \' \' \'{print $1}\') && docker rm $(docker ps -a  | awk  \'{print $1,$NF}\' | grep -v grep | grep \'kettle-admin\'| awk -F \' \' \'{print $1}\')'
            // echo 'ready to rmi kettle-admin-'+img_version
            // sh 'docker rmi `docker images | grep -v grep | grep \'kettle-admin\' | awk \'{print $3}\'`'
            //echo 'local image has deleted...!'

             sh 'cp -r  dist/* docker/kettle-admin/'
             def img_url = repo_url + '/' + name_space + '/' + img_name + ':' + img_version
             sh 'cd  docker/ && docker build -t ' + img_url + ' .'
             sh 'docker push ' + img_url
             echo 'ready to push it to aliyuncs....'
             echo 'ready to start  kettle-admin....'
             sh 'docker run --name '+img_name+' -p 80:80 -v /opt/apps/nginx/log:/var/log/nginx '+ ' -d '+img_url
             echo 'kettle-admin has started !'
         }

     }
